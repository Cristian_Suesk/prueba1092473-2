/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prueba1092473.pkg2;

/**
 *
 * @author SergioIván
 */
public abstract class FiguraGeometrica {
    
    private float area;
    private float perimetro;
    
    //Metodos abstractos, se declaran pero no se implementan
    
    public abstract float calcularArea();   
    public abstract float calcularPerimetro();
    
}
